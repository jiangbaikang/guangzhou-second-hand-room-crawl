import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

# 生成一组随机数据
data = np.random.randn(1000)
df = pd.read_csv(r'广州房价信息.csv')
info = df['区'].value_counts().index.to_list()
plt.rcParams['font.sans-serif'] = ['SimHei']  # 调用黑体字体
num = df['区'].value_counts().to_list()
x = info[0:125]
plt.figure(figsize=(30, 30), dpi=100)
y = num[0:125]
plt.xticks(fontsize=20)
plt.xlabel("小区数量现存楼盘数量",  loc="right",fontsize=20)
plt.barh(x, y)
plt.show()