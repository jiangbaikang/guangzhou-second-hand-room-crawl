import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv('广州房价信息.csv')
info = df['朝向'].value_counts().index.to_list()
plt.rcParams['font.sans-serif'] = ['SimHei']  # 调用黑体字体
num = df['朝向'].value_counts().to_list()
plt.pie(num[0:8], labels=info[0:8], autopct='%.2f%%')
plt.title('广州二手房朝向饼图')
plt.show()