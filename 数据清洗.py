import pandas as pd
# 对介绍栏数据清洗，导入csv数据
df = pd.read_csv(r'广州房价信息.csv')
#删除空格,排除用户输入格式多了空格而影响数据分析
df.fillna('', inplace=True)
print(df.to_string())