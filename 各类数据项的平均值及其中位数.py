import pandas as pd

# 对介绍栏数据清洗，导入csv数据
df = pd.read_csv(r'广州房价信息.csv')
# 计算每列的平均值
print("总价（万）众数为：", df['总价（万）'].mode()[0])
print("总价（万）中位数为：", df['总价（万）'].median())
print("总价（万）平均数为：", df['总价（万）'].mean())
print("面积（平米）众数为：", df['面积（平米）'].mode()[0])
print("面积（平米）中位数为：", df['面积（平米）'].median())
print("面积（平米）平均数为：", df['面积（平米）'].mean())
print("单价（元/平米）众数为：", df['单价（元/平米）'].mode()[0])
# print("单价（元/平米）中位数为：", df['单价（元/平米）'].median())
# print("单价（元/平米）平均数为：", df['单价（元/平米）'][0:8].mean())