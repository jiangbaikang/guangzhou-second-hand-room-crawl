# 绘制面积和总价的散点关系图
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

df = pd.read_csv(r'广州房价信息.csv')
home_area = df['面积（平米）'].apply(lambda x:float(x))
total_price = df['总价（万）']
plt.scatter(home_area,total_price,s=1)
plt.rcParams['font.sans-serif']=['SimHei']#调用黑体字体
plt.rcParams['axes.unicode_minus'] = False
plt.title('广州房价情况',fontsize=15)
plt.xlabel('房屋面积(平方米)',fontsize=15)
plt.ylabel('房价(万元)',fontsize=15)
plt.grid(linestyle=":", color="r")
plt.xlim((0, 300))
plt.ylim((0, 1500))
plt.show()