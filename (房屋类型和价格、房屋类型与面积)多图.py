import pandas as pd
from pylab import *

mpl.rcParams['font.sans-serif'] = ['SimHei']
mpl.rcParams['axes.unicode_minus'] = False
df = pd.read_csv(r'广州房价信息.csv')
# 房屋类型和价格的分析
plt.figure(figsize=(30, 10))
plt.subplot(1, 2, 1)  # 一行两列第一个图
type = df['房屋类型']
# type = list(type)
price = df['总价（万）']
plt.scatter(type, price)
plt.xlabel('房屋类型')
plt.ylabel('价格')

plt.subplot(1, 2, 2)  # 一行两列第一个图
plt.title('类型统计', fontsize=20, )
type.value_counts().plot(kind='bar', )  # 绘制条形图
plt.xlabel('房屋类型')
plt.show()